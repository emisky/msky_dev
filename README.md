**Сборка образа и загрузка его в hub.docker.com**
1. Скачиваем любым удобным способом файлы из репозитория, помещаем на хосте в произвольном каталоге. Я, на время опытов, использовал /var/www/
2. На хосте переходим в каталог со скачанными файлами
3. Создаем image следующей командой: `docker build -t msky-magic-www .`
4. Смотрим ID имейджа: `docker images`
5. Тэгируем имейдж: docker tag <id> <docker_login>/<repository_name>:<tag> <details><summary>Примечание</summary>
 в моем случае было так: docker tag <id> emichrist/msky-magic-www:latest
</details>

6. Авторизуемся на хабе: `docker login`
7. Пушим имейдж в хаб: `docker push emichrist/msky-magic-www`
