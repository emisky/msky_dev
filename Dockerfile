FROM node:alpine as builder
MAINTAINER Emi 'Migraine_Sky' Christ <emikhrist@gmail.com>

COPY . .

FROM nginx:alpine

RUN rm -rf /usr/share/nginx/html/*
COPY index.html /usr/share/nginx/html
COPY style.css /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf

RUN ln -sf /dev/stdout /var/log/nginx/access.log \ && ln -sf /dev/stderr /var/log/nginx/error.log

ENTRYPOINT ["nginx", "-g", "daemon off;"]
